# THIS MODULE MOVED TO https://gitlab.tails.boum.org/tails/puppet-yapgp

# yapgp

Yet another PGP puppet module.

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with yapgp](#setup)
    * [Beginning with yapgp](#beginning-with-yapgp)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module is designed to manage PGP public keys. It can fetch keys, keep
them up to date, set the trust level, and export them.

## Setup

### Beginning with yapgp

Just `include yapgp` and you're good to go (this will install ruby-gpgme).

## Usage

Typical usage would look like:

```
include yapgp

pgp_key { 'Tails sysadmins':
  fp     => 'D113CB6D5131D34BA5F0FE9E70F4F03116525F43':
  ensure => present,
  user   => root,
  trust  => 4,
}
```

This will add the tails-sysadmins@boum.org PGP public key to root's keyring
and set the owner trustlevel to 4.

To export a key, you can use the export_pgp_key function. Please keep in
mind that puppet functions are ran during catalog compilation, so regardless
of the relationship you specify, it will always run *before* any pgp_key
types are executed. Hence, to prevent exporting keys that are not present
in the keyring (yet), it is recommended to set a condition like this:

```
if export_pgp_key('D113CB6D5131D34BA5F0FE9E70F4F03116525F43') {
  file { '/root/tails-sysadmins.asc':
    content => export_pgp_key('D113CB6D5131D34BA5F0FE9E70F4F03116525F43','root'),
  }
}
```

## Limitations

This module assumes dirmngr is properly configured.

Only Debian 10 and higher are currently supported.

There is no support for dealing with private keys, nor for any encryption
or decryption of data.

## Development

Merge requests are welcome ;-)

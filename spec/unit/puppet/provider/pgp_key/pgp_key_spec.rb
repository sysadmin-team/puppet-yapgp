# frozen_string_literal: true

require 'spec_helper'

ensure_module_defined('Puppet::Provider::PgpKey')
require 'puppet/provider/pgp_key/pgp_key'

describe Puppet::Type.type(:pgp_key).provider(:pgp_key) do

  let(:provider) { resource.provider }
  let(:resource) do
    Puppet::Type.type(:pgp_key).new(
      ensure: :present,
      id: 'D113CB6D5131D34BA5F0FE9E70F4F03116525F43',
      trust: 1,
      user: 'groente',
      provider: described_class.name,
    )
  end

  describe 'create' do
    it 'imports a key' do
      expect(provider.create).to be_truthy
    end
  end

  describe 'destroy(context, name)' do
    it 'deletes a key' do
      expect(provider.destroy).to be_truthy
    end
  end
end

# frozen_string_literal: true

require 'gpgme' if Puppet.features.gpgme?

Puppet::Type.type(:pgp_key).provide(:pgp_key) do
  desc 'pgp_key provider for pgp_key resource'

  confine feature: :gpgme

  def exists?
    drop_to_user
    ctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_LOCAL })
    keys = ctx.keys(resource[:fp])
    revert_privs
    if keys.empty?
      return false
    end
    if resource[:ensure] == :absent
      return true
    end
    key = keys[0]
    if key.primary_subkey.expired
      return false
    end
    actual_trust = if key.owner_trust == 0
                     1
                   else
                     key.owner_trust
                   end
    actual_trust == resource[:trust]
  end

  def create
    drop_to_user
    ctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_EXTERN })
    thiskey = ctx.get_key(resource[:fp])
    GPGME.gpgme_op_import_keys(ctx, [thiskey])
    revert_privs
    trust = resource[:trust] + 1
    command = '/bin/echo "' + resource[:fp] + ':' + trust.to_s + ':" | /usr/bin/gpg --import-ownertrust'
    begin
      Puppet::Util::Execution.execute(command, uid: user_id, gid: group_id)
    rescue Puppet::ExecutionFailure
      raise Puppet::Error, "Could not set trust level for key #{resource[:fp]}"
    end
  end

  def destroy
    drop_to_user
    ctx = GPGME::Ctx.new({ protocol: GPGME::PROTOCOL_OpenPGP, keylist_mode: GPGME::KEYLIST_MODE_LOCAL })
    thiskey = ctx.get_key(resource[:fp])
    GPGME.gpgme_op_delete(ctx, thiskey, 1)
    revert_privs
  end

  def drop_to_user
    return if Process.euid == user_id
    begin
      Process::GID.grant_privilege(group_id)
      Process::UID.grant_privilege(user_id)
    rescue
      raise Puppet::Error, "Unable to run as user #{resource[:user]}"
    end
    ENV['HOME'] = Etc.getpwuid(Process.euid)['dir']
  end

  def revert_privs
    return if Process.euid == Process.uid
    Process::UID.switch
    Process::GID.switch
    ENV['HOME'] = Etc.getpwuid(Process.uid)['dir']
  end

  def user_id
    Etc.getpwnam(resource[:user]).uid
  end

  def group_id
    Etc.getpwnam(resource[:user]).gid
  end

  mk_resource_methods
end

# Changelog

All notable changes to this project will be documented in this file.

## Release 0.1.0

**Features**

  - The type `pgp_key`, which ensures a pgp key is up to date and at the right trustlevel.
  - The function `export_pgp_key`, which returns the armored public key.

**Bugfixes**

**Known Issues**
